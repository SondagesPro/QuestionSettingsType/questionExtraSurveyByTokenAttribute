# questionExtraSurveyByTokenAttribute

Access to survey via request on token attribute

## Usage

This plugin need reloadAnyReponse 5.7.0, questionExtraSurvey 5.4.0 and getQuestionInformation
You have settings in question and in reloadAnyResponse, you ,need to set both if you want user can reload survey uin the list. The 2 system (list and reload) can use different parameters.

### Question advanced settings attribute

In question advanced settinghs : on questionExtraSurvey part you have 3 new settings

- `extraSurveyTokenAttribute` attribute to be used for comparaison, can be the colun name or the attribute description
- `extraSurveyTokenAttributeQuestion` question code or sgqa for the comparaison done
- `extraSurveyTokenAttributeSeparator` optionnal separator for acces. No separator mean a complete comparaison, with a separator : the attribute value must be in the list

### Question advanced settings attribute

- `reloadViaTokenAttribute`  attribute to be used for comparaison,
- `reloadViaTokenAttributeColumn` question for comparaison
- `reloadViaTokenAttributeSeparator` separator
- `reloadViaTokenAttributeExtraRestriction` Extra restriction : One field by line, field must be a valid question code. Field and value are separated by colon (:), you can use < > etc … prefix.

