<?php

/**
 * Extend questionExtraSurvey and reloadAnyResponse to allow usage of token attribute
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <www.sondages.pro>
 * @copyright 2022 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class questionExtraSurveyByTokenAttribute extends PluginBase
{
    protected static $name = 'questionExtraSurveyByTokenAttribute';
    protected static $description = 'Extend questionExtraSurvey and reloadAnyResponse to allow usage of token attributes.';

    protected $storage = 'DbStorage';

    /* CDBCriteria to add to criteria (for listing) */
    private $criteria = null;

    public function init()
    {
        $this->subscribe('newQuestionAttributes', 'addExtraSurveyAttribute');
        $this->subscribe('questionExtraSurveyGetPreviousReponseCriteria', 'updateResponseListCriteria');
        $this->subscribe('ReloadAnyResponseAllowEdit');

        /* Settings */
        $this->subscribe('beforeRALSurveySettings');
        $this->subscribe('newReloadAnyResponseSurveySettings');
    }

    /**
     */
    public function addExtraSurveyAttribute()
    {
        if (!Yii::getPathOfAlias('questionExtraSurvey')) {
            return;
        }
        $extraAttributes = array(
            'extraSurveyTokenAttribute' => array(
                'types' => 'XT',
                'category' => $this->gT('Extra survey'),
                'sortorder' => 500, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->gT('You can use token attribute for relation on the list, you must update the related survey reload anyResponse settings.'),
                'caption' => $this->gT('Usage of token attribute.'),
            ),
            'extraSurveyTokenAttributeQuestion' => array(
                'types' => 'XT',
                'category' => $this->gT('Extra survey'),
                'sortorder' => 501, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->gT('The token attribute value must be find in this question.'),
                'caption' => $this->gT('Question fortoken attribute.'),
            ),
            'extraSurveyTokenAttributeSeparator' => array(
                'types' => 'XT',
                'category' => $this->gT('Extra survey'),
                'sortorder' => 502, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->gT('If value is empty : attribute value must be the value of the answers, else can be one value in the list.'),
                'caption' => $this->gT('Separator to find attribute value.'),
            ),
        );
        $this->getEvent()->append('questionAttributes', $extraAttributes);
    }

    /**
     * Update or replace criteria
     */
    public function updateResponseListCriteria()
    {
        $CriteriaEvent = $this->getEvent();
        $qid = $CriteriaEvent->get('qid');
        $token = $CriteriaEvent->get('token');
        $surveyId = $CriteriaEvent->get('surveyId');
        $oAttributeToken = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            array(":qid" => $qid, ":attribute" => 'extraSurveyTokenAttribute')
        );
        if (!$oAttributeToken or !$oAttributeToken->value) {
            return;
        }
        $oAttributeQuestion = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            array(":qid" => $qid, ":attribute" => 'extraSurveyTokenAttributeQuestion')
        );
        if (!$oAttributeQuestion or !$oAttributeQuestion->value) {
            return;
        }
        $criteria = $CriteriaEvent->get('criteria');
        if (empty($token)) {
            $criteria->addCondition('0 = 1');
            return;
        }
        $tokenAttributes = $this->getTokensAttributeList($surveyId);
        if (array_key_exists($oAttributeToken->value, $tokenAttributes)) {
            $tokenAttribute = $oAttributeToken->value;
        } else {
            $tokenAttribute = array_search($oAttributeToken->value, $tokenAttributes);
        }
        if (empty($tokenAttribute)) {
            \Yii::log("Invalid token attribute {$oAttributeToken->value} for question $qid.", \CLogger::LEVEL_ERROR, 'plugin.questionExtraSurveyByTokenAttribute.updateResponseListCriteria');
            $criteria->addCondition('0 = 2');
            return;
        }
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken)) {
            $criteria->addCondition('0 = 3');
            return;
        }
        $tokenvalue = $oToken->getAttribute($tokenAttribute);
        if (empty($tokenvalue)) {
            \Yii::log("Empty token value {$oAttributeToken->value} for question $qid and token $token.", \CLogger::LEVEL_WARNING, 'plugin.questionExtraSurveyByTokenAttribute.updateResponseListCriteria');
            $criteria->addCondition('0 = 4');
            return;
        }
        $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
        if (array_key_exists($oAttributeQuestion->value, $aColumnToCode)) {
            $questionCode = $oAttributeToken->value;
        } else {
            $questionCode = array_search($oAttributeQuestion->value, $aColumnToCode);
        }
        if (empty($questionCode)) {
            \Yii::log("Invalid question code {$oAttributeQuestion->value} for question $qid.", \CLogger::LEVEL_ERROR, 'plugin.questionExtraSurveyByTokenAttribute.updateResponseListCriteria');
            $criteria->addCondition('0 = 5');
            return;
        }
        $oAttributeSeparator = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            array(":qid" => $qid, ":attribute" => 'extraSurveyTokenAttributeSeparator')
        );
        $separator = "";
        if ($oAttributeSeparator) {
            $separator = $oAttributeSeparator->value;
        }
        $tokenCriteria = self::getAttributeCriteria($questionCode, $tokenvalue, $separator);
        $criteria->mergeWith($tokenCriteria);
    }

    /** Add te settings in beforeRALSurveySettings event**/
    public function beforeRALSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $tokensAttributes = $this->getTokensAttributeList($surveyId);
        if (empty($tokensAttributes)) {
            return;
        }
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $SurveySettingsEvent->set("settings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'reloadViaTokenAttribute' => array(
                    'type' => 'select',
                    'label' => $this->gt('Token attribute for testing access'),
                    'options' => $tokensAttributes,
                    'htmlOptions' => array(
                        'empty' => $this->gt("None"),
                    ),
                    'current' => $this->get('reloadViaTokenAttribute', 'Survey', $surveyId, '')
                ),
                'reloadViaTokenAttributeColumn' => array(
                    'type' => 'select',
                    'label' => $this->gt('Question for comparaison'),
                    'options' => $aQuestionList['data'],
                    'htmlOptions' => array(
                        'empty' => $this->gt("Disable"),
                    ),
                    'current' => $this->get('reloadViaTokenAttributeColumn', 'Survey', $surveyId, '')
                ),
                'reloadViaTokenAttributeSeparator' => array(
                    'type' => 'string',
                    'label' => $this->gt('Question for comparaison'),
                    'htmlOptions' => array(
                        'placeholder' => $this->gt("(single value)"),
                        'maxlenght' => 1
                    ),
                    'current' => $this->get('reloadViaTokenAttributeSeparator', 'Survey', $surveyId, '')
                ),
                'reloadViaTokenAttributeExtraRestriction' => array(
                    'type' => 'text',
                    'label' => $this->gT("Extra condition for token attribute reload."),
                    'default' => '',
                    'current' => $this->get('reloadViaTokenAttributeExtraRestriction', 'Survey', $surveyId, ""),
                    'help' => $this->gT('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
                ),
            )
        ));
    }

    /** Save the reloadAnyResponse settings **/
    public function newReloadAnyResponseSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $settings = $SurveySettingsEvent->get('settings');
        foreach ($settings as $setting => $value) {
            $this->set($setting, $value, 'Survey', $surveyId);
        }
    }

    /** Check if user is allowed to edit via  ReloadAnyResponseAllowEdit event**/
    public function ReloadAnyResponseAllowEdit()
    {
        $ReloadAnyResponseEvent = $this->getEvent();
        $surveyId = $ReloadAnyResponseEvent->get('surveyId');

        $reloadViaTokenAttribute = $this->getStaticSurveySettings($surveyId, 'reloadViaTokenAttribute');
        $reloadViaTokenAttributeColumn = $this->getStaticSurveySettings($surveyId, 'reloadViaTokenAttributeColumn');
        $reloadViaTokenAttributeSeparator = $this->getStaticSurveySettings($surveyId, 'reloadViaTokenAttributeSeparator');
        if (!$reloadViaTokenAttribute || !$reloadViaTokenAttributeColumn) {
            return;
        }
        $srid = $ReloadAnyResponseEvent->get('srid');
        /* Current token */
        if ($ReloadAnyResponseEvent->get('when') == 'getUrl') {
            $currentSurveyid = App()->getRequest()->getParam('sid', App()->getRequest()->getParam('surveyid'));
            if ($currentSurveyid) {
                $token = \reloadAnyResponse\Utilities::getCurrentToken($currentSurveyid);
            } else {
                /* Must not happen : send log error ? */
                $token = $ReloadAnyResponseEvent->get('token');
            }
        } else {
            /* token set by plugin */
            $token = $ReloadAnyResponseEvent->get('token');
        }
        if (!$token || !\reloadAnyResponse\Utilities::checkIsValidToken($surveyId, $token)) {
            return false;
        }

        if (empty($this->criteria)) {
            $oToken = Token::model($surveyId)->findByToken($token);
            if (empty($oToken) || empty($oToken->getAttribute($reloadViaTokenAttribute))) {
                \Yii::log("Empty token or token value {$reloadViaTokenAttribute} for survey $surveyId with token {$token}.", \CLogger::LEVEL_ERROR, 'plugin.questionExtraSurveyByTokenAttribute.ReloadAnyResponseAllowEdit');
                return;
            }
            $tokenValue = $oToken->getAttribute($reloadViaTokenAttribute);
            $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
            if (array_key_exists($reloadViaTokenAttributeColumn, $aColumnToCode)) {
                $questionCode = $reloadViaTokenAttributeColumn;
            } else {
                $questionCode = array_search($reloadViaTokenAttributeColumn, $aColumnToCode);
            }
            if (empty($questionCode)) {
                \Yii::log("Invalid question code {$reloadViaTokenAttributeColumn} for survey $surveyId.", \CLogger::LEVEL_ERROR, 'plugin.questionExtraSurveyByTokenAttribute.ReloadAnyResponseAllowEdit');
                return;
            }
            $extraRestriction = $this->getStaticSurveySettings($surveyId, 'reloadViaTokenAttributeExtraRestriction');
            $this->criteria = \reloadAnyResponse\Utilities::getResponseCriteria($surveyId, null, $extraRestriction);
            $tokenCriteria = self::getAttributeCriteria($questionCode, $tokenValue, $reloadViaTokenAttributeSeparator);
            $this->criteria->mergeWith($tokenCriteria);
        }
        $allowedCriteria = new CDbCriteria();
        $allowedCriteria->compare("id", $srid);
        $allowedCriteria->mergeWith($this->criteria);
        $oResponse = SurveyDynamic::model($surveyId)->find($allowedCriteria);
        if (!empty($oResponse)) {
            $ReloadAnyResponseEvent->set('allowed', true);
        }
    }

    /**
     * get the token attribute list
     * @param integer $surveyId
     * @param string $prefix
     * @return array
     */
    private function getTokensAttributeList($surveyId, $prefix = "", $default = false)
    {
        if (Yii::getPathOfAlias('TokenUsersListAndManagePlugin')) {
            return \TokenUsersListAndManagePlugin\Utilities::getTokensAttributeList($surveyId, $prefix, $default);
        }
        $aTokens = array();
        if ($default) {
            $aTokens = array(
                $prefix . 'firstname' => gT("First name"),
                $prefix . 'lastname' => gT("Last name"),
                $prefix . 'token' => gT("Token"),
                $prefix . 'email' => gT("Email"),
            );
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokens[$prefix . $attribute] = $attribute;
            if (!empty($information['description'])) {
                $aTokens[$prefix . $attribute] = $information['description'];
            }
        }
        return $aTokens;
    }

    /**
     * Get the criteria to be merged
     * @param string $questionCode
     * @param string $tokenValue
     * @param string $separator
     * @return \CDbCriteria
     */
    private static function getAttributeCriteria($questionCode, $tokenValue, $separator)
    {
        $separator = trim($separator);
        $tokenCriteria = new CDbCriteria();
        $quotedColumnName = \App()->getDb()->quoteColumnName($questionCode);
        if ($separator === '') {
            $tokenCriteria->compare($quotedColumnName, $tokenValue);
        } else {
            $separator = mb_substr($separator, 0, 1);
            $separator = self::escapeSqlString($separator); // Escape the separator
            $tokenCriteria->compare($quotedColumnName, $tokenValue); // IS the value
            $tokenValue = self::escapeSqlString($tokenValue); // Escape the value
            $tokenCriteria->compare($quotedColumnName, $tokenValue . "{$separator}%", true, 'OR', false); // Start by
            $tokenCriteria->compare($quotedColumnName, "%{$separator}" . $tokenValue . "{$separator}%", true, 'OR', false); // contain
            $tokenCriteria->compare($quotedColumnName, "%{$separator}" . $tokenValue, true, 'OR', false); // End by
        }
        return $tokenCriteria;
    }

    /**
     * Sql string must be escaped diffrently
     * @see https://github.com/yiisoft/yii2/blob/master/framework/db/mssql/conditions/LikeConditionBuilder.php
     * @param string $string
     * @return string
     */
    private static function escapeSqlString($string)
    {
        $escapingReplacements = [
            '%' => '\%',
            '_' => '\_',
            '\\' => '\\\\',
        ];
        if (in_array(App()->db->driverName, ['sqlsrv', 'dblib', 'mssql'])) {
            $escapingReplacements = [
                '%' => '[%]',
                '_' => '[_]',
                '[' => '[[]',
                ']' => '[]]',
                '\\' => '[\\]',
            ];
        }
        return strtr($string, $escapingReplacements);
    }

    /**
     * Get the survey related settings
     * Used as static due to multiple call in some situation
     * @since 0.2.1
     * @param integer $surveyId
     * @param string $settings
     * @return mixed
     */
    private function getStaticSurveySettings($surveyId, $setting, $global = false, $default = "")
    {
        static $settings = [];
        if (!isset($settings[$surveyId])) {
            $settings[$surveyId] = [];
        }
        if (!isset($settings[$surveyId][$setting])) {
            if ($global) {
                $default = $this->get($setting, null, null, $this->settings[$setting]['default'] ?? $default);
            }
            $settings[$surveyId][$setting] = $this->get($setting, 'Survey', $surveyId, $default);
        }
        return $settings[$surveyId][$setting];
    }
}
